# Programação II : National Oceanic and Atmospheric Administration #

Dupla : Israel Efraim de Oliveira, José Carlos Zancanaro.

### O que é este repositório? ###

Este repositório guarda um trabalho para a disciplina de Programação II no curso de Bacharelado em Ciência da Computação da UNIVALI.

### Conteúdos utilizados ###

* Interface Gráfica
* Banco de dados
* SQL

### Ambiente ###

* Netbeans
* Xampp
* MySQL
